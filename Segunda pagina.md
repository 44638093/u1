# Introduction to Scrum 
[![Introduction to Scrum](https://i.ytimg.com/an_webp/9TycLR0TqFA/mqdefault_6s.webp?du=3000&sqp=CMC3yPsF&rs=AOn4CLBh8_xubUTKMxhHSgOrNDg7Ulicyw)](https://www.youtube.com/watch?v=9TycLR0TqFA)

### **En que consiste**
Scrum es un framework de desarrollo agil adaptable a cada proyecto y circunstancia que permite entregar valor rápidamente.
Para poder entregar resultados rápidos, Scrum divide el trabajo en sprints que suelen ser de dos semanas y al final del sprint debe entregarse una funcionalidad terminada.

[Introduction](https://gitlab.com/44638093/u1/-/blob/master/README.md)| [Ventajas](https://gitlab.com/44638093/u1/-/blob/master/Tercera%20pagina.md) | 
| --- | ---
