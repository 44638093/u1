# SCRUM

![texto altenativo](https://www.tuleap.org/wp-content/uploads/2020/06/Scrum-process-schema-EN-small.png)

## Como Funciona

_Es un proceso en el que se aplican de manera regular un conjunto de buenas prácticas para trabajar colaborativamente, en equipo y obtener el mejor resultado posible de proyectos, caracterizado por_

1. Adoptar una estrategia
2. Basar la calidad del resultado
3. Solapar las diferentes fases del desarrollo

### Roles 

|     **Scrum Master**    |  Product Owner  |    Team |
| ---|---| --- |
|  Procura facilitar la aplicación de Scrum <br> y gestionar cambios  </br> | Que representa a los stakeholders <br> (interesados externos o internos) </br >|Ejecuta el desarrollo y demás elementos <br>elacionados con él</br>|

  [VideoTutorial](https://gitlab.com/44638093/u1/-/blob/master/Segunda%20pagina.md)| [Ventajas](https://gitlab.com/44638093/u1/-/blob/master/Tercera%20pagina.md) | 
| --- | ---
